# Assignment 2

All solutions must be placed on [CodeAnywhere](https://codeanywhere.com) into the folder `a02`.
Put each problem into the corresponding file `p1.py`, `p2.py`, `p3.py` *etc*.

All files must produce *no errors* when invoked with Python *e.g.* as
```
python p1.py
```




### Problem 1

Use a for loop to print the following sequence of integers, in reverse order —
`2`, `8`, `14`, `20`, `26`, `32`




### Problem 2

A year is a leap year if it is divisible by 4, unless it is a century that is
not divisible by 400. Write a function `IsLeapYear(year)` that takes a year as
a parameter and returns `True` if the year is a leap year, and `False` otherwise




### Problem 3

Write a function that takes three integers as parameters and returns the largest




### Problem 4

A fruit company sells oranges for 40 cents a pound plus $8.50 per order for shipping.
If an order is over 100 pounds, shipping cost is reduced by $2.50.
On top of that, if the overall cost is at least $100, the client receives an 8% discount.
Write a function that will take the number of pounds as a parameter and return the cost of the order

